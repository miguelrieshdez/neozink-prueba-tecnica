<?php
/*
    $infoCover['products'][n]->id
    $infoCover['products'][n]->name
    $infoCover['products'][n]->price
    $infoCover['products'][n]->button_text
    $infoCover['products'][n]->button_link
    $infoCover['products'][n]->image
----
    $infoCover['slide'][n]->id
    $infoCover['slide'][n]->title
    $infoCover['slide'][n]->button_text
    $infoCover['slide'][n]->button_link
    $infoCover['slide'][n]->bg_image

*/

include "lib/config.php";
include "templates/header.php";
?>
<header class="header">
    <div class="header__logo">
        <img src="./resources/logo.png" alt="Neoshop" title="Neoshop" />
    </div><!-- .header__logo -->
    <div class="header__shop shop">
        <a href="#" class="shop__button">
            <span class="shop__icon"><i class="fas fa-shopping-cart"></i></i></span>
            <span class="shop__text">Carrito</span>
        </a><!-- .shop__button -->
        <a href="#" class="shop__button">
            <span class="shop__icon"><i class="fas fa-user"></i></span>
            <span class="shop__text">Mi perfil</span>
        </a><!-- .shop__button -->
        <a href="#" class="shop__button">
            <span class="shop__icon"><i class="fas fa-box-open"></i></i></span>
            <span class="shop__text">Mis pedidos</span>
        </a><!-- .shop__button -->
    </div><!-- .header__shop -->
</header>
<div class="wrapper">
    <div class="slider">
        <div class="slider__arrow prev">
            <span class="fas fa-angle-left"></span>
        </div>
        <?php
            $nItemsSlider = count($infoCover['slide']);
            for ($i=0; $i < $nItemsSlider; $i++) { 
        ?>
                <div class="slider__item item <?php if($i ==0) echo 'active'?>" style="background-image: url('<?php echo($infoCover['slide'][$i]->bg_image);?>')">
                    <div class="container">
                        <h2><?php echo($infoCover['slide'][$i]->title);?></h2>
                        <a href="<?php echo($infoCover['slide'][$i]->button_link);?>"><?php echo($infoCover['slide'][$i]->button_text);?></a>
                    </div>
                </div>
        <?php
            }
        ?>
        <div class="slider__pause-play pause">
            <span class="far fa-pause-circle"></span>
            <span class="far fa-play-circle"></span>
        </div>
        <div class="slider__arrow next">
            <span class="fas fa-angle-right"></span>
        </div>
    </div><!-- .slider -->
    <div class="products container">
        <h2>Productos</h2>
        <?php
            echo helper_products_list($infoCover['products']);
        ?>
    </div><!-- .products -->
    <div class="load">
        <!-- TODO: BONUS. Desarrollar la funcionalidad para que el botón cargue más productos -->
        <span class="load__button button effect1">Cargar más productos</span>
    </div><!-- .load -->
</div><!-- .wrapper -->
<footer class="footer">
    <p><img src="./resources/logo.png" alt="Neoshop" title="Neoshop" /></p>
    <p>Prueba técnica para candidatos</p>
</footer>
<?php
include "templates/footer.php";
