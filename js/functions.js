/**
 * TODO: Desarrollar el código del slider
 */
 function nextItem(){
    activeItem = $('.slider__item.active');
    nextItemToActive = $('.slider__item.active').next('.slider__item');

    if(nextItemToActive.length > 0){
        activeItem.toggleClass("active").next('.slider__item').toggleClass("active");
    }else{
        activeItem.toggleClass("active")
        $('.slider__item').first().toggleClass("active");
    }
 }
 function prevItem(){
    activeItem = $('.slider__item.active');
    prevItemToActive = $('.slider__item.active').prev('.slider__item');

    if(prevItemToActive.length > 0){
        activeItem.toggleClass("active").prev('.slider__item').toggleClass("active");
    }else{
        activeItem.toggleClass("active")
        $('.slider__item').last().toggleClass("active");
    }
 }

 var intervalSlider;

 function initIntervalSlider(){
    window.intervalSlider = setInterval(() => { 
        nextItem() 
    }, 5000);
 }

initIntervalSlider();

$(".slider__arrow.next").on('click', function() {
	nextItem();
    clearInterval(window.intervalSlider);
    initIntervalSlider();
});
$(".slider__arrow.prev").on('click', function() {
	prevItem();

    clearInterval(window.intervalSlider);
    initIntervalSlider();
});
$(".slider__pause-play").on('click', function() {
    if($(this).hasClass('play')){
        initIntervalSlider();
        $(this).addClass('pause');
        $(this).removeClass('play');
    }else{
        clearInterval(window.intervalSlider);
        $(this).removeClass('pause');
        $(this).addClass('play');
    }
    
});


/**
 * TODO: Desarrollar el código para cargar productos por Ajax
 */

$('.load__button').on('click', function () {
    $.ajax({
        type: "GET",
        url: "https://localhost/neoshop/lib/config.php",
        crossDomain: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        data : {
           function: "get", 
           type: "products", 
        },

        success: function(response) {
            $('.products.container').append(response)
        }
    });
});