<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');


/**
 * TODO: Configura la aplicación en base a tus necesidades
 */
define('WEB_PATH', 'neoshop/');
define('ENDPOINT_URL', 'http://localhost/neoshop/api/');
define('PROTOCOL', 'http');

define('BASE', PROTOCOL . '://' . $_SERVER['HTTP_HOST'] . '/' . WEB_PATH);

include_once "functions.php";

// Variable con la informacion usada en la cover
$infoCover = array(
	'products' => get('products')->data,
	'slide' => get('slides')->data
);

if ( isset($_GET['function']) && !empty(isset($_GET['function'])) ) {
	$type = $_GET['type'];
	$function = $_GET['function'];
	switch ($function) {
		case 'get':
			if($type == 'products'){
				$info = get('products')->data;
				$layoutProducts = helper_products_list($info);
			}else if($type == 'slide'){
				$info = get('slides')->data;
			}
			break;
		
		default:
			break;
	}
	echo $layoutProducts;
}
