<?php
/**
 * Perform a GET request to the API endpoint
 *
 * @param  string $method with the url to to the request
 *
 * @return array  with the result
 */
function get($method)
{
  /**
   * TODO: Desarrolla un método para obtener los resultados de los ficheros JSON. Preferiblemente utilizando cURL.
   */
  $endpoint = ENDPOINT_URL.$method.".json";

 	$ch = curl_init();
	if ($ch) {

		//Se establece el endpoint
	   	curl_setopt($ch, CURLOPT_URL, $endpoint);

		//Se establecen las cabeceras
	   	$headers= array('Content-Type:application/json');
       	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//Refrescamos la conexión
       	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

		//Proceso sin verificaciones
       	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		//Nos devuelve la informacion como texto
       	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
       	$response = curl_exec($ch);

       	if (!$response) {
       	    die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
       	}else{
       	    //  decodificamos el json
       	    $response = json_decode($response);
       	}
       	curl_close($ch);
   }
   return $response;
}


/**
 * Perform a GET request to the API endpoint
 *
 * @param  array $data with the info products
 *
 * @return string  with the HTML
 */
function helper_products_list($data){
  $code = "";
  $nItemsProduct = count($data);
  for ($i=0; $i < $nItemsProduct; $i++) { 
    if($i == 7){
      $code .= '<div class="products__item special-item" style="background-image: url(\'./resources/cta/cta2.jpg\')">
                </div>';   
    }
    $code .='<div class="products__item item" data-id-product="'.$data[$i]->id.'">
                <div class="item__image image" style="background-image: url('.$data[$i]->image.')"></div>
                <div class="item__info info">
                    <span class="info__name name">'.$data[$i]->name.'</span>
                    <span class="info__price price">'.$data[$i]->price.'</span>
                </div>
                <div class="item__btn">
                    <span class="button effect1">'.$data[$i]->button_text.'</span>
                </div>
            </div>';
    if($i == 3){
      $code .= '<div class="products__item special-item" style="background-image: url(\'./resources/cta/cta1.jpg\')">
                </div>';
    }
  }
  return $code;
}

